
function auth() {
    var config = {
        'client_id': getPhpData('clientId'),
        'scope': getPhpData('feedsLink')
    };
    gapi.auth.authorize(config, function() {
        fetch(gapi.auth.getToken());

    });
}

function fetch(token) {
    $.ajax({
        url: getPhpData('getContacts') + token.access_token + "&alt=json",
        dataType: "jsonp",
        success:function(data) {
            $.ajax({
                type:"post",
                url:"/user/import-contacts",
                data: data,
                success: function(response){
                    var friends = $('.friends_div');
                    friends.append(response.html_blocks);
                }
            })

        }
    });
}