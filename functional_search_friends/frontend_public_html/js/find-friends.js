$(document).ready(function(){

    $('#get-friends').keyup(function(){
        var item = $('#get-friends').val();
        if(!item) return;
        $.ajax({
            url:'/user/get-friends',
            method: 'post',
            data: {data: item},
            success: function(response){
                $('.profile-page__all-friends').replaceWith(response.html_blocks);
            }
        })
    });

    $('ul.title-tabs--friends').on('click', 'li', function(){
        if (!$(this).hasClass('active')) {
            $('#friends-search-result').remove();
            $('#invite-mail').removeAttr("style");
        }
    })
});

$("#all-friends").on("change", function () {
    var $firend_search_check = $(".profile-page__friend__check").siblings("input[type='checkbox']");
    if ($(this).prop("checked")) {
        $firend_search_check.prop("checked", true);
    } else {
        $firend_search_check.prop("checked", false);
    }
});
//show more
$(document).on('click', '.bt-all-users', function (e) {

    var item = $('#get-friends').val();
    var limit = $(this).data('limit');
    if(!item) return;
    $.ajax({
        url:'/user/get-friends?limit=' + limit,
        method: 'post',
        data: {data: item},
        success: function(response){
            $('.profile-page__all-friends').replaceWith(response.html_blocks);
        }
    });

    e.preventDefault();
});

$(document).on('click', '.js-check-friend', function (e) {
    if($(this).hasClass('checked')){
        $(this).removeClass('checked');
    }else{
        $(this).addClass('checked');
    }
});

$(document).on('click', '.js-add-friends', function (e) {

    var personIds = [];

    $('.profile-page__friend').each(function (i) {
     var input = $(this).children('input');
        if (input.hasClass('checked')) {
            personIds.push(input.data('userid'));
        }
    });

    if (personIds.length == 0) {
        return false;
    }

    $.ajax({
        url: '/friendship/add-friends',
        method: 'POST',
        data: {personIds: personIds},
        success: function () {
            window.location.reload();
        }
    })
});

$(document).on('click', '.js_add_row_email_friend', function () {

    var container = $(this).closest('.container_email_friends'),
        template = $(container).find('.main-content__find-friends__invitation__item').first(),
        $fragment = $(template).clone(true),
        rowNumber = $(".main-content__find-friends__invitation__item").length - 2;

    $fragment.attr("id", "email-friend-" + rowNumber).attr("name", "email" + "[" + rowNumber + "]" + "[email]").val('');
    $(this).before($fragment);

});

$(document).on('click', '#all-friends', function () {
    $('input:checkbox').not(this).prop('checked', this.checked).addClass('checked');

    $('input:checkbox').change(function(){
        if($(this).is(":checked")) {
            $('.js-check-friend').addClass("checked");
        } else {
            $('.js-check-friend').removeClass("checked");
        }
    });
});