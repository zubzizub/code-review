<?php

class UserController extends Controller_Site
{
    /** @var Frontend_LayoutResources_Container */

    private $container = null;

    const LIMIT_START_FINDFRIENDS = 15;
    const LIMIT_ADD_FINDFRIENDS = 15;
    const COUNT_USERS = 'countUsers';

    public function findFriendsAction()
    {
        $this->notAjaxInit();
        $this->view->doctype('XHTML1_RDFA');
        $collectorLocator = $this->getCollectorLocator();
        $collector = $collectorLocator->createUserFriendsCollector();
        $collector->collectTo($this->container);
        $config = Zend_Registry::get('config');
        B_JsContainer::addData('clientId', $config->gmail->clientId);
        B_JsContainer::addData('feedsLink', $config->gmail->feedsLink);
        B_JsContainer::addData('getContacts', $config->gmail->getContacts);

        $user = $this->container->get(ResourceDefinitions::USER_REGISTERED);

        $this->container->addResource(ResourceDefinitions::USER_FRIENDS, $followers);
        $form = new Site_Form_SendFriendsForm();
        $this->view->form = $form;

        if (!$this->getParam(Site_Form_SendFriendsForm::SUBMIT)) {
            return null;
        }
        $params = $this->getAllParams();
        $formIsValid = $form->isValid($params);

        if ($formIsValid) {
            $addressMail = [];
            foreach ($form->getCollection(Site_Form_SendFriendsForm::EMAIL) as $subForm) {
                $addressMail[] = $subForm->getElement(Site_Form_SendFriendsForm::EMAIL)->getValue();
            }
            $emailInfo['user_name'] = $user['name'];
            $emailInfo['to_address'] = $addressMail;
            $emailInfo['message'] = $params['message'];
            $this->addMailQueue($emailInfo);
        }

        if (!$formIsValid) {
            $errorsCollector = new Frontend_LayoutResources_Collector_FormErrors($form);
            $errorsCollector->collectTo($this->container);
            $this->assignContainerToView($this->container);
            return;
        }

        $this->assignContainerToView($this->container);
    }

    public function importContactsAction()
    {
        $this->ajaxInit();
        $collectorLocator = $this->getCollectorLocator();
        $collector = $collectorLocator->createRegisteredUserCollector();
        $collector->collectTo($this->container);
        $user = $this->container->get(ResourceDefinitions::USER_REGISTERED);

        if (!$user) {
            return false;
        }

        $this->_request->setParam('id', $user['id']);
        $collectorLocator = $this->getCollectorLocator();
        $collector = $collectorLocator->createUserFindGmailCollector();
        $collector->collectTo($this->container);
        $this->assignContainerToView($this->container);
        $users = $this->container->get(ResourceDefinitions::USER_FIND_GMAIL);
        $this->view->users = $users;
        $response['html_blocks'] = $this->view->render('user/partials/friends-list.phtml');

        return $this->returnJson($response);
    }

    public function getFriendsAction()
    {
        $this->ajaxInit();
        $collectorLocator = $this->getCollectorLocator();
        $collector = $collectorLocator->createRegisteredUserCollector();
        $collector->collectTo($this->container);
        $user = $this->container->get(ResourceDefinitions::USER_REGISTERED);
        $this->_request->setParam('id', $user['id']);
        $this->container->addResource(
            'limit',
            $this->getParam('limit') ? $this->getParam('limit') : self::LIMIT_START_FINDFRIENDS
        );
        $collectorLocator = $this->getCollectorLocator();
        $collector = $collectorLocator->createUserFindCollector();
        $collector->collectTo($this->container);
        $this->assignContainerToView($this->container);
        $countUsers = $this->container->get(self::COUNT_USERS);

        if ($countUsers) {
            $limit = $this->getParam('limit', self::LIMIT_START_FINDFRIENDS);
            if ($countUsers > $limit) {
                $this->view->nextLimit = $limit + self::LIMIT_ADD_FINDFRIENDS;
            }
        }

        $users = $this->container->get(ResourceDefinitions::USER_FIND);

        if (!$users) {
            return false;
        }

        $this->view->users = $users;
        $response['html_blocks'] = $this->view->render('user/partials/friends-list.phtml');
        return $this->returnJson($response);
    }

    public function addMailQueue($emailInfo)
    {
        $data = [];
        $options = [];
        $data['username'] = $emailInfo['user_name'];
        $data['text'] = $emailInfo['message'];
        $mailer = new Mail_Mailer('invite_friends', $data, $options);

        $mailer->set([
            'from' => Config::val('email'),
            'to' => $emailInfo['to_address'],
            'fromname' => Config::val('from_name')
                ? Config::val('from_name')
                : ''
        ]);

        $mailer->queueForSend();
    }
}