<?php
class FriendshipController extends Controller_Site
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function addFriendsAction()
    {
        $user = B_Registered::getInstance()->getUser();
        $userId = $user['id'];
        $personIds = $this->getParam('personIds');
        if (empty($personIds)) {
            return $this->returnJson([
                'success' => false,
                'text'  => B_T::_("Invalid request")
            ]);
        }
        $userData = [];
        $friendsModel = new Site_Model_User_Friends();
        $result = $friendsModel->sendFriendsRequest($userId, $personIds, $userData);
        // Add notification
        if ($result != Site_Model_User_Friends::REQUEST_ALREADY_SENT) {
            if (!empty($userId) && !empty($userData[$userId]['name'])) {
                $notificationProcessor = new Frontend_Notifications_Pusher();
                $notificationProcessor->push(
                    Frontend_Notifications_Entities_NewFriend::TYPE,
                    $personIds,
                    ['friend_id' => $userId, 'friend_name' => $userData[$userId]['name']]
                );
            }
        }
        return $this->returnJson(array(
            'text'  =>  B_T::_('Request to add as friend sent'),
            'class' =>  Messanges::STYLE_SUCCESS
        ));
    }
}