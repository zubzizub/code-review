<?php
class Site_Form_SendFriendsForm extends B_Form
{
    const EMAIL = 'email';
    const MESSAGE = 'message';
    const SUBMIT = 'submit';
    public function init()
    {
        $emailGroup = new B_Form_Element_Group();

        $emailGroup->addElement((new Zend_Form_Element_Text(self::EMAIL, [
            'id' => 'email-friend',
            'class'=>'main-content__find-friends__invitation__item',
            'required' => true,
            'placeholder'=>'ivanov@email.com',
            'filters'=>[
                'stringTrim'
            ],
            'validators'=>[
                'EmailAddress'
            ],
        ]))->setAttrib('required', 'required')->setDecorators([
            'Viewhelper'
        ]));

        $this->addCollection($emailGroup, self::EMAIL, [
            'dynamic' => true,
            'max' => 5,
            'count' => 1,
            'shrink' => true,
            'renumber' => true
        ]);

        $this->addElement((new Zend_Form_Element_Textarea(self::MESSAGE, [
            'tabindex' => '2',
            'rows'=>'6',
            'cols'=>'20',
            'placeholder'=>B_T::_('Hello, let s be friends!'),
            'class'=>'main-content__find-friends__invitation__item default',
            'id' => 'social',
            'filters' => [
                'stringTrim',
                'htmlEntities'
            ],
        ]))->setValue(B_T::_('Hello, let s be friends')));

        $this->addElement(new Zend_Form_Element_Button(self::SUBMIT, [
            'decorators' => ['ViewHelper'],
            'type' => 'submit',
            'label' => B_T::_('Send'),
            'value' => 'submit',
            'class' => 'button button-medium button-transparent orange',
        ]));

        foreach ($this->getFormElements() as $element) {
            if ($element->isRequired()) {
                $element->setAttrib('required', 'required');
            }
        }

        $t = B_T::getInstance();
        $langObj = B_Language::getInstance();
        $currLang = $langObj->getStoreLanguage();
        $translator = new Zend_Translate_Adapter_Array($t->getErrorsArray(), $currLang);
        $this->setTranslator($translator);
    }

    public function isValid($data)
    {
        $isValid = parent::isValid($data);
        return $isValid;
    }

    /**
     * @return Zend_Form_Element[]
     */
    private function getFormElements()
    {
        return $this->getElements();
    }
}