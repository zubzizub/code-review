<?php
class Site_Model_Users extends Bd_Table
{
    protected $_name = 'users';
    protected static $_instance = null;

    /**
     * @return Site_Model_Users
     */
    public static function getInstance()
    {
        return parent::getInstance();
    }

    public function getUsersByEmails($emails, $regUserId){
        $select = $this->getAdapter()->select();
        $select = $select->from('users as u', [
            '*', 'u.id as search_id',
            new Zend_Db_Expr(
                '(SELECT COUNT(id)
                FROM `user_friends` uf
                WHERE uf.user_id = ' . $regUserId . '
                AND friend_id = search_id) AS is_friend'
            )
        ])
            ->where('email IN (?)', $emails)
            ->where('u.active=?', 1)
        ;
        $users = $this->fetchFromSelect($select);
        return Model_FieldExtractor::getKeyValueMap('id', $users);
    }
}