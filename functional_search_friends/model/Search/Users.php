<?php
class Site_Model_Search_Users extends Bd_Table
{
    protected $_name = 'search_users';
    protected static $instance;

    /**
     * @return Site_Model_Search_Users
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

     public function findUsersByName($nameUser, $regUserId = null, $limit = null)
    {
        $select = $this->getAdapter()->select();
        $select = $select->from('search_users as su', [
            '*',
            'su.id as search_id',
            new Zend_Db_Expr('(SELECT COUNT(id) FROM `user_friends` uf
                                WHERE uf.user_id = ' . $regUserId . '
                                AND friend_id = search_id
                                AND friend_status != "CANCELLED") AS is_friend')
        ])
            ->where('su.name=?', $nameUser)
            ->where('su.id !=?', $regUserId);

        if (is_int($limit)) {
            $select->limit($limit);
        }
        $users = $this->fetchFromSelect($select);

        return Model_FieldExtractor::getKeyValueMap('id', $users);
    }

    public function findUsersByEmail($email, $regUserId = null, $limit = null)
    {
        $select = $this->getAdapter()->select();
        $select = $select->from('users as u', [
            '*',
            'u.id as search_id',
            new Zend_Db_Expr('(SELECT COUNT(id) FROM `user_friends` uf
                WHERE uf.user_id = ' . $regUserId . '
                AND friend_id = search_id
                AND friend_status != "CANCELLED") AS is_friend')
        ])
            ->where('u.email=?', $email)
            ->where('u.id !=?', $regUserId);

        if (is_int($limit)) {
            $select->limit($limit);
        }
        $users = $this->fetchFromSelect($select);
        return Model_FieldExtractor::getKeyValueMap('id', $users);
    }
}
