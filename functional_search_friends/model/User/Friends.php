<?php
class Site_Model_User_Friends extends Bd_Table implements Site_Model_Exporter_StatisticCollector
{
    protected $_name = 'user_friends';
    protected static $_instance = null;

    const REQUEST_SENT = 2;
    const FRIEND_PENDING = 'pending';

    /**
     * @return Site_Model_User_Friends
     */
    public static function getInstance()
    {
        return parent::getInstance();
    }

    public function sendFriendsRequest($userId, $friendsIds, &$userData)
    {
        $userModel = new Site_Model_Users();
        $usersIds = $friendsIds;
        $usersIds[] = $userId;
        $userData = $userModel->getUsersListByIds($usersIds);
        $status = self::REQUEST_SENT;
        foreach ($friendsIds as $friendsId) {
            $this->insert([
                'user_id' => $userId,
                'friend_id' => $friendsId,
                'friend_status' => self::FRIEND_PENDING
            ]);
        }
        return $status;
    }
}