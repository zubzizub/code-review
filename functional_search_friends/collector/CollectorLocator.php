<?php
class CollectorLocator
{
    /**
     * @var Zend_Controller_Request_Abstract
     */
    private $request;

    /**
     * @param Zend_Controller_Request_Abstract $request
     */
    public function __construct(Zend_Controller_Request_Abstract $request)
    {
        $this->request = $request;
    }
    /**
     * @return Collector_UserFindGmail
     */
    public function createUserFindGmailCollector()
    {
        $key = 'user-find-gmail';
        if (!$this->getCachedCollector($key)) {
            $this->cacheCollector(
                new Collector_UserFindGmail(
                    $this->request,
                    $this->createRegisteredUserCollector()
                ),
                $key
            );
        }
        return $this->getCachedCollector($key);
    }

    /**
     * @return Collector_UserFind
     */
    public function createUserFindCollector()
    {
        $key = 'user-find';
        if (!$this->getCachedCollector($key)) {
            $this->cacheCollector(
                new Collector_UserFind($this->request, $this->createRegisteredUserCollector()),
                $key
            );
        }
        return $this->getCachedCollector($key);
    }
}