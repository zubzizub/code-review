<?php
class Collector_UserFind extends Frontend_LayoutResources_Collector_CollectorCached {
    /**
     * @var Zend_Controller_Request_Abstract
     */
    private $request;
    private $registeredUserCollector;
    const LIMIT = 15;
    /**
     * @param Zend_Controller_Request_Abstract $request
     */
    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Frontend_LayoutResources_Collector_User $collector
    ) {
        $this->request = $request;
        $this->registeredUserCollector = $collector;
    }
    /**
     * @param Frontend_LayoutResources_Container $container
     */
    public function collectDataForCache(Frontend_LayoutResources_Container $container)
    {
        $this->registeredUserCollector->collectTo($container);
        $regUser = $container->get(self::USER_REGISTERED);

        if (!$regUser) {
            return false;
        }

        $regUserId = (int)$regUser['id'];
        $userId = intval($this->request->getParam('id'));

        if ($userId < 1) {
            return false;
        }

        $limit = ($this->request->getParam('limit'))?$this->request->getParam('limit'):self::LIMIT;
        $whatFind = trim(strval($this->request->getParam('data')));

        if (!$whatFind) {
            return false;
        }

        $modelSearchUsers = Site_Model_Search_Users::getInstance();

        if (filter_var($whatFind, FILTER_VALIDATE_EMAIL)) {
            $users = $modelSearchUsers->findUsersByEmail($whatFind, $regUserId, $limit);
            $container->addResource('countUsers', count($modelSearchUsers->findUsersByEmail($whatFind, $regUserId)));
        } else {
            $users = $modelSearchUsers->findUsersByName($whatFind, $regUserId, $limit);
            $container->addResource('countUsers', count($modelSearchUsers->findUsersByName($whatFind, $regUserId)));
        }

        if (empty($users)) {
            return false;
        }

        $statModel = new Site_Model_Statistic();
        $statData = $statModel->getStatisticsByStatTypesAndObjectIds([
            Site_Model_Statistic::STAT_REVIEWS_WRITTEN,
            Site_Model_Statistic::STAT_FRIENDS,
            Site_Model_Statistic::STAT_FOLLOWERS,
            Site_Model_Statistic::STAT_FOLLOWERS_FOR_YOU
        ], array_keys($users));

        if (!empty($statData)) {
            foreach ($statData as $item) {
                $userId = $item['iid'];
                $paramKey = $item['uid'];
                $paramValue = $item['count'];
                $users[$userId][$paramKey] = $paramValue;
            }
        }
        $container->addResource(self::USER_FIND, $users);
    }
    /**
     * @return array
     */
    protected function getResourceNamesToCheckIfCollected()
    {
        return [self::USER_FIND];
    }
}
