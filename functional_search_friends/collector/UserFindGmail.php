<?php
class Collector_UserFindGmail extends Frontend_LayoutResources_Collector_CollectorCached {
    /**
     * @var Zend_Controller_Request_Abstract
     */
    private $request;
    private $registeredUserCollector;
    /**
     * @param Zend_Controller_Request_Abstract $request
     */
    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Collector_User $collector
    ) {
        $this->request = $request;
        $this->registeredUserCollector = $collector;
    }
    /**
     * @param Frontend_LayoutResources_Container $container
     */
    public function collectDataForCache(Frontend_LayoutResources_Container $container)
    {
        $userId = intval($this->request->getParam('id'));
        if ($userId < 1) {
            return false;
        }

        $this->registeredUserCollector->collectTo($container);
        $regUser = $container->get(self::USER_REGISTERED);

        if (!$regUser) {
            return false;
        }

        $regUserId = (int)$regUser['id'];
        $dataWithGmail = $this->request->getParam('feed');
        $contacts = [];

        if ($dataWithGmail) {
            foreach ($dataWithGmail['entry'] as $contact) {
                if (isset($contact['gd$email'])) {
                    $contacts[] = $contact['gd$email'][0]['address'];
                }
            }
        }

        $userModel = Site_Model_Users::getInstance();
        $users = $userModel->getUsersByEmails($contacts, $regUserId);

        if (empty($users)) {
            return false;
        }

        $statModel = new Site_Model_Statistic();
        $statData = $statModel->getStatisticsByStatTypesAndObjectIds([
            Site_Model_Statistic::STAT_REVIEWS_WRITTEN,
            Site_Model_Statistic::STAT_FRIENDS,
            Site_Model_Statistic::STAT_FOLLOWERS,
            Site_Model_Statistic::STAT_FOLLOWERS_FOR_YOU
        ], array_keys($users));

        if (!empty($statData)) {
            foreach ($statData as $item) {
                $userId = $item['iid'];
                $paramKey = $item['uid'];
                $paramValue = $item['count'];
                $users[$userId][$paramKey] = $paramValue;
            }
        }
        $container->addResource(self::USER_FIND_GMAIL, $users);
    }
    /**
     * @return array
     */
    protected function getResourceNamesToCheckIfCollected()
    {
        return [self::USER_FIND_GMAIL];
    }
}
