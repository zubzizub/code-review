<?php
interface ResourceDefinitions
{
    const USER_FIND = 'user-find';
    const USER_FIND_GMAIL = 'user-find-gmail';
    const USER_REGISTERED = 'user-registered';
    const USER_FRIENDS = 'user-friends';
}
